from django.shortcuts import render, HttpResponse
from django.conf import settings
from django.http import JsonResponse
from .models import Excel
from books.models import Book
from realtors.models import Realtor
from django.utils import timezone

import pandas as pd
import numpy as np
import requests
from threading import Thread
from io import StringIO
import json
import time

from openpyxl import load_workbook
from io import BytesIO

"""
  Decorator function to start a new thread
"""
def start_new_thread(function):
    def decorator(*args, **kwargs):
        t = Thread(target=function, args=args, kwargs=kwargs)
        t.daemon = True
        t.start()
    return decorator

# This is my attemp to be the main method/ Route
@start_new_thread
def fetchNewData(request, debug = False):
    if (debug):
        debugFetchNewData()
    # The file to searchFile
    searchQuery = "FileTestFromCodigo"
    # search in dropbox for the searQuery
    searchResponse = searchFile(searchQuery, debug)
    # parse the datetime of the first match that got in the response
    strDatetime = searchResponse["matches"][0]["metadata"]["server_modified"]
    # Compare if the datetime of the file is different from the last time
    isTheFileModified = compareDatetime(strDatetime,debug)
    if (isTheFileModified):
        print("fetchNewData: Archivo Modificado")
        # Check what is modified in the file
        compareTwoFileDf(debug)

    return JsonResponse(isTheFileModified, safe=False)
    # return HttpResponse(date_modified_present)

# Below are helper functions used in the above route
'''
 Helper method use in fetchNewData function. This method is used to print some debug
 information
'''
def debugFetchNewData():
    print("debugFetchNewData: Latest Datetime from db ",getLatestDateTimeFromDb())
    print ("debugFetchNewData: Latest dataframe from db: ", getLatestFileDfFromDb())

'''
  Helper method use in fetchNewData route. it search for a file in dropbox
'''
def searchFile(nameOfFile: str, debug = False):
    '''
        This function search file in dropbox.

        :param nameOfFile: The name of the file to search 
        :type nameOfFile: string
        :return: The response in Json format
    '''
    url = "https://api.dropboxapi.com/2/files/search"
    headers = {
        "Authorization": "Bearer B54iI51sVOIAAAAAAAAAIvznsQP2LKP3rZXSVTtcVMu1fAB6LjI5jPls_B1tEDU1",
        "Content-Type": "application/json"
    }
    data = {
        "path": "",
        "query": nameOfFile
    }
    r = requests.post(url, headers=headers, data=json.dumps(data))
    responseJsonData = r.json()
    if (debug):
        print("searchFile: Json Response: \n", 
        json.dumps([responseJsonData], indent=4, sort_keys=True))
    return responseJsonData

'''
  Used in the main route - fetchNewData to compare two DateTime
'''
def compareDatetime(strDatetime, debug = False):

    date_modified_past = getLatestDateTimeFromDb()
    date_modified_present = strDatetime
    if (debug):
        print(f"compareDatetime: datetime past: {date_modified_past}")
        print(f"compareDatetime: datetime present: {date_modified_present}")
    # If both string are not empty check if they are equal
    if (date_modified_past and date_modified_present):
        if not (date_modified_past == date_modified_present):
            print("compareDatetime: Saving dateTime")
            datePastDb = Excel(date_modified_past = date_modified_present)
            datePastDb.save()
            return True  # La fecha de los archivos ha cambiado
        else:
            return False  # La fecha de los archivos no ha cambiado
    else:
        print("compareDatetime: One of the Datetime was empty. Most likely datetime past. Running again the method")

    return False  # Una de las dos fechas fue empty o None(null)

# Get the latest values that is not null
# obj= Model.objects.filter(date_modified_past__isnull = True).latest('date_modified_past')

'''
  Helper method used in compareDatetime and compareTwoFileDF function. It do what it say
'''
def getLatestDateTimeFromDb(debug = False):
    latest_datetime_notNull = Excel.objects.filter(date_modified_past__isnull = False).latest('date_modified_past')
    latest_datetimeStr = timezone.localtime(latest_datetime_notNull.date_modified_past).strftime("%Y-%m-%dT%H:%M:%SZ")
    if (debug):
        print(f"getLatestDateTimeFromDb: datetime coming from db {latest_datetimeStr}")
    return latest_datetimeStr

'''
  Helper method used in compareDatetime and compareTwoFileDF function. It do what it say
'''
def getLatestFileDfFromDb(debug = False):
    latest_file_notNull = Excel.objects.filter(old_fileStr__isnull = False)  
    latest_file_notNull = list(latest_file_notNull)[-1].old_fileStr 
    # print("latest file not null: ",latest_file_notNull)
    latestFileDf = pd.read_csv(StringIO(latest_file_notNull))
    if (debug):
        print("getLatestFileDfFromDb: Latest dtaframe from db: ", latestFileDf.to_string())
    return  latestFileDf


'''
   Used in the main route - fecthNewData to compare two file and verify 
   what is modified in the file which is done in another helper method
'''
@start_new_thread
def compareTwoFileDf(debug = False):
    old_fileDf = getLatestFileDfFromDb()
    present_fileDF1 = download()
    sheet_name = present_fileDF1.sheet_names[0]
    dataFrame = present_fileDF1.parse(sheet_name)
    present_fileDf = dataFrame
    # Check if dataframe is not empty 
    if not (old_fileDf.empty):
        if not (present_fileDf.equals(old_fileDf)):
            if (debug):                
                print("compareTwoFileDf: Los dos archivos se suponen que no sean iguales")
                print("compareTwoFileDf: old File pandas: ", old_fileDf.to_string())
                print("compareTwoFileDf: present File pandas: ", present_fileDf.to_string())
            # Save the DateTime in the Database for comparing later
            datePastDb = Excel(old_fileStr = present_fileDf.to_csv(index = False))
            datePastDb.save()
            print("compareTwoFileDf: Fechas Guardadas en base de dato")
            detectChanges(present_fileDf, old_fileDf, debug = True)
    else:
        print("compareTwoFileDf: olfd_fileDF was empty")

    # print(peta_length)
    # print(present_fileDf.equals(old_fileDf))


# me quede aqui. detectar cambios en dataframe
def detectChanges(newFile: pd.DataFrame, oldFile: pd.DataFrame, debug = False):
    # Cambiar los Not a Number(NaN) to the number 0
    newFile = newFile.fillna(0)
    oldFile = oldFile.fillna(0)
    if (debug):
        print(f"\n detectChanges: El nuevo archivo que contiene los cambios  \n {newFile.head()}")
        print(f"\n detectChanges: El archivo del pasado  \n {oldFile.head()}\n")
    # Verify is new row is added when excel is edited
    isNewDataAdded(newFile, oldFile)
    # verify what roe is change in the excel
    valuesChangedInFile = isDataUpdated(newFile, oldFile, debug)
    # Save the new data to the Database 
    saveNewDataToDB(valuesChangedInFile, debug)

'''
 Method used in detectChanges. Verificar si unos de los dos archivos tiene data nueva añadida.
'''
def isNewDataAdded(newFile: pd.DataFrame, oldFile: pd.DataFrame):
    # Tengo que detectar si hay una nueva columna pq si hay nueva columna la añado ya a la base de datos y no tengo que compararla. Si no hay ninguna columna nueva añadida pues entonces verifico si algo cambio. Puedo crear eso en metodos
    pass;

'''
 Method used in detectChanges. Verificar si hay datos modificados en los dos archivos.
'''
def isDataUpdated(newFile: pd.DataFrame, oldFile: pd.DataFrame, debug = False):
    
    # This two lines of code is to know which columns was updated
    ne_stacked = (oldFile != newFile).stack()
    columns_changed = ne_stacked[ne_stacked] 

    difference_locations = np.where(oldFile != newFile)
    changed_from = oldFile.values[difference_locations]
    changed_to = newFile.values[difference_locations]
    
    index2 = difference_locations[0]
    data = changed_to.tolist()
    columnName1 = newFile.iloc[index2]['title']

    # print(f"changeName:\n{changeName}")
    if (debug):
        print(f"isDataUpdated: Index data modified:{difference_locations[0][0]}")
        print(f"isDataUpdated: Index data type:{type(difference_locations[0])}")
        print(f"isDataUpdated: columns title of the data modified:{columnName1}")
    valuesChangedInFile = pd.DataFrame({'title':columnName1.values,'from': changed_from, 'to': changed_to}, index = columns_changed.index)
    columnsChanged = valuesChangedInFile.index.to_frame()
    if (debug):
        print(f"isDataUpdated: columns Changed:{columnsChanged}")

    columnName = columnsChanged.values[:,1].tolist()
    columnId = columnsChanged.values[:,0].tolist()
    selectRow = newFile.loc[newFile[columnName[0]].isin(data)]
    # no duplicate row
    rowChangedTitle = selectRow.loc[selectRow.index.isin(columnId)]['title']


    if(debug):
        print(f"\n isDataUpdated: La fila que cambio: \n {valuesChangedInFile}")
        print(f"isDataUpdated: La columna que modifique \n{columnName}\n")
        print(f"isDataUpdated: Los indice de la fila en el dataframe: \n{columnId}\n")
        print(f"isDataUpdated: La data que modifique: \n{data}")
        print(f"isDataUpdated: El row que modifique: \n{rowChangedTitle}")
        print(f"isDataUpdated: Total de columna que modifique: {columnsChanged.shape[0]}\n\n")
    
    return valuesChangedInFile

def saveNewDataToDB(newFile: pd.DataFrame, debug = False):
    # Get all row the first values that is the name of the columns. The zero values is the index of the columns
    dataFrameColumsChanged = newFile.index.to_frame()
    columnsChanged = dataFrameColumsChanged.values
    if (debug):
        print(f"saveNewDataToDB: Row que se va a guardar en la base de datos: \n {newFile}")
        print(f"saveNewDataToDB: Column changed: \n {columnsChanged}")

    indices = dataFrameColumsChanged.values[:,0]
    columns = dataFrameColumsChanged.values[:,1]
    # uniqueIndice = np.unique(indices)
    realtor = createRealtor()

    for counter,index in enumerate(indices):   
        if (debug):
            print(f"\n\nsaveNewDataToDB: Indice del row que se esta guardando {index}")  
            print(f"saveNewDataToDB: Counter {counter}")  
        data = newFile.loc[index]
        title = data['title'].values[0]
        if (debug):
             print(f"saveNewDataToDB: title del indice {index}: {title}")
        propiedades = data.index.values
        datos = data['to'][propiedades[counter]]
        firstColumns = propiedades[counter]
        if (debug):
            print(f"saveNewDataToDB: propiedades: {propiedades}")
            print(f"saveNewDataToDB: first Columns: {firstColumns}")
            print(f"saveNewDataToDB: Datos First Column: \n{datos}")
        # only for logging purpose. 
        newData = {firstColumns:datos}
        print(f"saveNewDataToDB: dictionario a guardar en base de datos:\n{newData}")
        # update a book or create a book. Search for a title and update or create by the newData
        book, bookCreated = Book.objects.update_or_create(
            title=title,
            defaults={firstColumns:datos, 'realtor':realtor}
        )



# Nuevos metodos que no tienen  que ver con el algoritmo principal de Excel

@start_new_thread
def longWait():
    print("Entering a Long wait simulated")
    for item in range(0, 10):
        time.sleep(1)
        if item == 9:
            print("end of looop")

def createRealtor():
    # How to create a new object in database from code
    realtor = Realtor(name="persona en el codigo",
                      phone="787-334-333", email="aa@hotmail.com")
    realtor.save()
    return realtor
"""
 The download takes times is better if I do this Asynchronously to not block
 the UI for the user
"""
# @start_new_thread
def download():
    fileUrl = 'https://www.dropbox.com/s/bp83npagpmfui48/FileTestFromCodigo.xlsx?raw=1'
    file = pd.ExcelFile(fileUrl)
    return file

'''
Route/Endpoint Method
'''
def uploadFileToDropbox(request):
    '''
    https://dropbox.github.io/dropbox-api-v2-explorer/#files_upload
    This api explorer generate this code
    '''
    url = "https://content.dropboxapi.com/2/files/upload"

    headers = {
        "Authorization": "Bearer " + settings.DROPBOX_TOKEN,
        "Content-Type": "application/octet-stream",
        "Dropbox-API-Arg": "{\"path\":\"/FileTestFromCodigo.xlsx\",\"mode\":{\".tag\":\"overwrite\"}}"
    }
    # Esto se repite abajo file
    url_file = 'https://www.dropbox.com/s/8erg8l44p3ovt5q/FileTestCloud.xlsx?raw=1'
    file = requests.get(url_file, allow_redirects=True)
    # data = open(file.content, "rb").read()
    r = requests.post(url, headers=headers, data=file.content)
    print(r)
    # ################ READ Workbook
    # wb=load_workbook(filename=BytesIO(file.content))
    # sheet = wb.active
    # print (sheet['F1'].value)
    # sheet['F2'] = "Titulo hecho en codigo from python"
    # wb.save("FileTestFromCodigo.xlsx")


def excelToDb(request):
    xl = download()
    sheet_name = xl.sheet_names[0]
    df1 = xl.parse(sheet_name)
    print (df1.to_string())
    peta_length = df1['Petal Length']
    Sepal_Width = df1['Sepal Width']
    Sepal_Lenght = df1['Sepal Length']
    isbn = df1['ISBN']
    titulo = df1['Titulo']
    departarmento = df1['Departamento']
    cantidad = df1['Cantidad']

    print("excelToDb: " + peta_length)
    print("excelToDb: " + Sepal_Lenght)
    print("excelToDb: "+Sepal_Width)
    print("excelToDb: "+isbn)
    print("excelToDb: "+titulo)
    print("excelToDb: "+departarmento)
    print("excelToDb: "+cantidad)

    # Change Excel cell
    # titulo[0] = "Changing a cell in Pandas DB"
    # Save Excel To Local file with PD
    saveExcelToLocalFile()

    # How to create a new object in database from code
    # realtor = Realtor(name="persona en el codigo",
    #                   phone="787-334-333", email="aa@hotmail.com")
    # realtor.save()
    # book = Book(realtor=realtor, title="titulo del libro en codigo",
    #             category="Salud", price=5, photo_main="photos/2018/10/28/ComunicaLibroDelete.jpg")
    # book.save()
    print('LOG: book must be saved in database')
    # return redirect(index)


def saveExcelToLocalFile():
    writer = pd.ExcelWriter('XLSexcel.xls')
    df1.to_excel(writer, sheet_name)
    writer.save()
    print("saveExcelToLocalFile - LOG: Finally Save excel to local project")



def saveToDropbox():
    # global date_modified_past
    # global date_modified_present

    print("saveToDropbox: printing date from past")
    print(date_modified_past)
    print("saveToDropbox: Date from present in server")
    print(date_modified_present)
    print("saveToDropbox: testing if theyre equal ', date_modified_past == date_modified_present")

    url = "https://content.dropboxapi.com/2/files/upload"

    headers = {
        "Authorization": "Bearer " + settings.DROPBOX_TOKEN,
        "Content-Type": "application/octet-stream",
        "Dropbox-API-Arg": "{\"path\":\"/testDirectory/FileTestFromCodigo.xlsx\",\"mode\":{\".tag\":\"overwrite\"}}"
    }
    # Esto se repite abajo file
    # url_file = 'https://www.dropbox.com/s/8erg8l44p3ovt5q/FileTestCloud.xlsx?raw=1'

    # file = requests.get(url_file, allow_redirects=True)
    data = open('./testDirectory/FileTestFromCodigo.xlsx', "rb").read()
    # dbug
    # print (data)
    # debug
    r = requests.post(url, headers=headers, data=data)  # file.content
    jsonr = r.json()
    date_modified_past = date_modified_present  # swap values,global variable
    date_modified_present = jsonr['server_modified']
    print("saveToDropbox: Date from server present")
    print(date_modified_present)
    print("saveToDropbox: date from past ", date_modified_past)


# ######### Wtach Dogg
import sys
import logging
from watchdog.observers import Observer
from watchdog.events import LoggingEventHandler, FileSystemEventHandler
class EventHandler(FileSystemEventHandler):
    def on_any_event(self, event):
        print("EVENT")
        print(event.event_type)
        print(event.src_path)
        print()
        saveToDropbox()


@start_new_thread
def watchfordog():
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
    path = './testDirectory'
    event_handler = EventHandler()
    observer = Observer()
    observer.schedule(event_handler, path, recursive=True)
    observer.start()
    time.sleep(120)
    observer.stop()
    observer.join()
    print("End listening for chagening file ")


# --------------------------  #
# Metodos que no quiero borrar pero no los uso en este archivo 


# --------------------------- #

# Code That work y que no quiero borrar
# me quede aqui. detectar cambios en dataframe
def detectChanges1(file1: pd.DataFrame, file2: pd.DataFrame, debug = False):
    if (debug):
        print(f"{file1.head()}")
        print(f"{file2.head()}")
    # detectar si hay una actualizacion. e.g si no hay mas row 
    # detectar si se añadio un nuevo record 
    # detectar si se hicieron las dos cosas. actualizacion y añadir

    # Esta tambien  funciona pero el segundo metodo es mas bonito pq me dice mas cosas
    # dfChangesBoolean2 = (file2 != file1).any(1)
    # dfChangesBoolean = (file2 != file1)
    # file1 = file1[dfChangesBoolean]
    dfConcat = pd.concat([file1,file2], 
                   axis='columns', keys=['First', 'Second'])
    df_SwapLevel = dfConcat.swaplevel(axis='columns')[file1.columns[1:]]
    df_SwapLevel[(file1 != file2).any(1)].style.apply(highlight_diff, axis=None)
    print(f"detecChanges:  Boolean Dataframe: {df_SwapLevel}")


'''
 metodo que no uso pero highlit de amarillo la diferencia
 Highlights the difference between two DataFrame
'''
def highlight_diff(data, color='yellow'):
    attr = 'background-color: {}'.format(color)
    other = data.xs('First', axis='columns', level=-1)
    return pd.DataFrame(np.where(data.ne(other, level=0), attr, ''),
                        index=data.index, columns=data.columns)
 